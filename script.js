const loader = document.querySelector(".loader");
const body = document.querySelector("body");
const modal = document.querySelector(".modal");
const btnAdd = document.querySelector(".btnAdd");
const form = {
  title: document.querySelector(".inputTitle"),
  text: document.querySelector(".inputText"),
  btnSave: document.querySelector(".btnSave"),
};
let users = [];
let postList = document.querySelector("#posts");
let posts = [];
let statusModal = "closed";
let searchUser = (idUser) => {
  let correctUser;
  users.forEach((user) => {
    if (user.id === idUser) {
      correctUser = {
        name: user.name,
        email: user.email,
      };
    }
  });
  return correctUser;
};
const handleSave = () => {
  let newPublication = {
    id: posts[posts.length - 1].id + 1,
    userId: 1,
    title: form.title.value,
    body: form.text.value,
  };
  posts = [{ ...newPublication }, ...posts];
  form.title.value = "";
  form.text.value = "";
  modal.style.display = "none";
  statusModal = "closed";
  btnAdd.innerText = "Add new publication";
  generatePosts();
  fetch(`https://ajax.test-danit.com/api/json/posts`, {
    method: "POST",
  });
};
let setEditButtons = () => {
  let btnsEdit = document.querySelectorAll(".edit");
  btnsEdit.forEach((edit) => {
    edit.addEventListener("click", () => {
      let currentId = parseInt(edit.id.substring(5));
      let btnSavePost = document.querySelector(`#save-${currentId}`);
      btnSavePost.style.display = "block";
      edit.style.display = "none";
      const postBody = document.querySelector(`#body-${currentId}`);
      const bodyText = postBody.innerText;
      postBody.innerHTML = `
            <textarea id ="change-${currentId}" placeholder="Write new post text"></textarea>
            `;
      btnSavePost.addEventListener("click", () => {
        const newText = document.querySelector(`#change-${currentId}`);
        if (newText.value.length === 0) {
          postBody.innerHTML = bodyText;
          generatePosts();
        } else {
          fetch(`https://ajax.test-danit.com/api/json/posts/${currentId}`, {
            method: "PUT",
            body: JSON.stringify({ body: newText.value }),
          })
            .then((res) => res.json())
            .then((data) => {
              posts = posts.filter((post) => {
                if (post.id == data.id) {
                  post.body = data.body;
                }
                return post;
              });
              generatePosts();
            });
        }
      });
    });
  });
};
let setDeleteToBtns = () => {
  let btnsDelete = document.querySelectorAll(".btnDelete");
  btnsDelete.forEach((btnItem) => {
    btnItem.addEventListener("click", () => {
      let currentId = parseInt(btnItem.id.substring(7));
      fetch(`https://ajax.test-danit.com/api/json/posts/${currentId}`, {
        method: "DELETE",
      }).then(() => {
        posts = posts.filter((post) => post.id !== currentId);
        generatePosts();
      });
    });
  });
};
class Card {
  constructor(name, email, title, body, id) {
    (this.name = name),
      (this.email = email),
      (this.title = title),
      (this.body = body),
      (this.id = id);
  }
}
let generatePosts = () => {
  postList.innerHTML = "";
  posts.forEach((post) => {
    let card = new Card(
      searchUser(post.userId).name,
      searchUser(post.userId).email,
      post.title,
      post.body,
      post.id
    );
    // let postTowrite = {
    //     name: searchUser(post.userId).name,
    //     email: searchUser(post.userId).email,
    //     title:post.title,
    //     body:post.body
    // }
    postList.innerHTML += `
        <div class="post">
            <div class="userInfo">
                <p>${card.name}</p>
                <p>${card.email}</p>
            </div>
            <p class="postTitle">${card.title}</p>
            <p id="body-${card.id}">${card.body}</p>
            <div class="btnsPost">
                <button class="btnSavePost" id="save-${card.id}">
                    Save
                </button>
                <button class="edit" id="edit-${card.id}"></button>
                <button class="btnDelete" id="delete-${card.id}">Delete</button>
            </div>
            
        </div>
        `;
  });
  setEditButtons();
  setDeleteToBtns();
};
fetch("https://ajax.test-danit.com/api/json/users")
  .then((res) => res.json())
  .then((data) => {
    users = [...data];
    fetch("https://ajax.test-danit.com/api/json/posts")
      .then((res) => res.json())
      .then((data) => {
        setTimeout(() => {
          loader.style.display = "none";
          body.style.height = "auto";
          body.style.alignItems = "flex-start";
          btnAdd.style.display = "block";
          form.btnSave.addEventListener("click", handleSave);
          btnAdd.addEventListener("click", () => {
            if (statusModal == "closed") {
              modal.style.display = "block";
              statusModal = "open";
              btnAdd.innerText = "Close modal window";
            } else {
              modal.style.display = "none";
              statusModal = "closed";
              btnAdd.innerText = "Add new publication";
            }
          });
          posts = [...data];
          generatePosts();
        }, 3000);
      });
  });

/*
1  Получение USERS
2  Получить POST
3  Вывод постов

    */
